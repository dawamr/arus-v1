<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Counter;

class CounterControllers extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ]);
    }

    public function index()
    {
        try {
            $counter = Counter::paginate(10);
            
            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function create(Request $request){
        try {
            $counter = new Counter;
            $counter->fill($request->all());
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function update(Request $request, $id){
        try {
            $counter = Counter::find($id);
            $counter->fill($request->all());
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function destroy(){
        try {
            $counter = Counter::find($id);
            $counter->delete();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }
}
