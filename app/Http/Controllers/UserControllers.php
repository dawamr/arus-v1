<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\User;
use \App\Role;

class UserControllers extends Controller
{

    public function index(Request $request)
    {
        try {
            $user = User::whereRoleIs('user')->get();      
            $message = "success";
            $row = $user;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|string|min:8|max:255|confirmed',
            'password_confirmation' => 'required|string|min:8|max:255',
            'phone' => 'required|string|min:10|unique:users,phone,NULL,id,deleted_at,NULL',
            'nik' => 'integer|min:10|unique:users,nik,NULL,id,deleted_at,NULL'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'messages' => $validator->messages()
            ], 400);
        }

        try {
            $user = new User;
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->save();
            $user->attachRole(2);

            $message = "success";
            $row = $user;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function show($id)
    {
        try {
            $data = User::whereRoleIs('user')->find($id);
            $message = "success";
            $row = $user;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:100',
            'email' => 'email|max:255',
            'nik' => 'numeric|max:100',
            'phone'=> 'string|max:15|min:9',
        ]);
        if($validator->fails()) {
            $message = "error";
            $row = $validator->messages();
            $code = 400;
            return $this->ok($message, $row, $code);
        }
        try {
            $data = User::whereRoleIs('user')->find($id);
            $data->name = $request->get('name');
            $data->email = $request->get('email');
            $data->phone = $request->get('phone');
            $data->nik = $request->get('nik');
            $data->save();

            $message = "success";
            $row = $data;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function destroy($id)
    {
        try {
            $data = User::whereRoleIs('user')->find($id);
            $data->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

}
