<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\UserFamilyMember as Family;

class FamilyControllers extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ]);
    }

    public function index($user_id)
    {
        try {
            $family = Family::where('user_id',$user_id)->get();
            
            $message = "success";
            $row = $family;
            $code = 200;
        
        } catch (\Throwable $th) {
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
